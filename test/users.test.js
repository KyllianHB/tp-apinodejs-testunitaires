import chai from 'chai';
import chaiHttp from 'chai-http';
import api from '../index.js';

chai.use(chaiHttp);

// IMPORTANT : For Mocha working, always use function () {}
// (never () => {})


describe('Users', function () {
  
  // --- READ USER TEST

  it('GET /users should return a success response with all users', function (done) {
    chai.request(api)
    .get('/users')
    .end((_, res) => {
      chai.expect(res.statusCode).to.equal(200);
      chai.expect(res.body).to.deep.equal({
        data: [
          {
            id: '1',
            lastName: 'Menvusa',
            firstName: 'Gérard', 
            birthDate: '1998-08-06',
            address: '3 rue du fun 44000 Nantes',
            phone: '0614587598',
            email: 'gerardmenvusa@gmail.com',
          }
        ]
      });
      done();
    });
  });

  // --- CREATE USER TEST ---

  it('POST /users should create the user and return a success response with the user', function (done) {
    const user = {
      lastName: 'Drier',
      firstName: 'Thibault',
      birthDate: '1962-01-25',
      adress: '3 rue de l escalade',
      phone: '+33645847963',
      email: 'thibaultdrier@gmail.com'
    };
    chai.request(api)
    .post('/users')
    .send(user)
    .end((_, res) => {
      chai.expect(res.statusCode).to.equal(201);
      chai.expect(res.body).to.deep.equal({
        data: user
      });
      done();
    });
  });
  it('POST /users with not correct birth date', function (done) {
    const user = {
      lastName: 'Drier',
      firstName: 'Thibault',
      birthDate: '1962-01-25',
      adress: '3 rue de l escalade',
      phone: '+33645847963',
      email: 'thibaultdrier@gmail.com'
    };
    chai.request(api)
    .post('/users')
    .send(user)
    .end((_, res) => {
      chai.expect(res.statusCode).to.equal(201);
      chai.expect(res.body).to.deep.equal({
        data: user
      });
      done();
    });
  });
  it('POST /users with not correct phone number', function (done) {
    const user = {
      lastName: 'Drier',
      firstName: 'Thibault',
      birthDate: '05-02-1986',
      adress: '3 rue de l escalade',
      phone: '111111111',
      email: 'thibaultdrier@gmail.com'
    };
    chai.request(api)
    .post('/users')
    .send(user)
    .end((_, res) => {
      chai.expect(res.statusCode).to.equal(422);
      done();
    });
  });

  // --- DELETE USER TEST ---
  it('DELETE /users/:id should delete the user and return a success response', function (done) {
    const userId = 1;
    chai.request(api)
    .delete(`/users/${userId}`)
    .end((_, res) => {
    chai.expect(res.statusCode).to.equal(204);
    done();
    });
  });
      
  // --- UPDATE USER TEST ---    
  it('PUT /users/:id should update the user and return a success response with the updated user', function (done) {
    const userId = 1;
    const updatedUser = {
      id: userId,
      lastName: 'UpdatedLastName',
      firstName: 'UpdatedFirstName',
      birthDate: '1990-01-01',
      address: '123 Updated Street, Updated City',
      phone: '0685789685',
      email: 'updated.email@example.com'
    };
    chai.request(api)
    .put(`/users/${userId}`)
    .send(updatedUser)
    .end((_, res) => {
      chai.expect(res.statusCode).to.equal(200);
      chai.expect(res.body).to.deep.equal({
      data: {
        id: `${userId}`,
        ...updatedUser
      } 
      });
      done();
    });
  });
  it('PUT /users/:id with wrong number should return 422', function (done) {
    const userId = 1;
    const updatedUser = {
      id: userId,
      lastName: 'UpdatedLastName',
      firstName: 'UpdatedFirstName',
      birthDate: '1990-01-01',
      address: '123 Updated Street, Updated City',
      phone: '+120255587',
      email: 'updated.email@example.com'
    };
    chai.request(api)
    .put(`/users/${userId}`)
    .send(updatedUser)
    .end((_, res) => {
      chai.expect(res.statusCode).to.equal(422);
      chai.expect(res.body).to.deep.equal({
      data: "Phone format not correct"
      });
      done();
    });
  });
  it('PUT /users/:id with wrong number should return 422', function (done) {
    const userId = 1;
    const updatedUser = {
      id: userId,
      lastName: 'UpdatedLastName',
      firstName: 'UpdatedFirstName',
      birthDate: '25-12-2500',
      address: '123 Updated Street, Updated City',
      phone: '0685789685',
      email: 'updated.email@example.com'
    };
    chai.request(api)
    .put(`/users/${userId}`)
    .send(updatedUser)
    .end((_, res) => {
      chai.expect(res.statusCode).to.equal(422);
      chai.expect(res.body).to.deep.equal({
      data: "Date format not correct"
      });
      done();
    });
  });
});