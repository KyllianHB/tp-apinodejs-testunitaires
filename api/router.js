
export default (controller, app) => {
  app.get('/books', controller.bookCtrl.listBooks);
  app.post('/books', controller.bookCtrl.createBook);
  app.get('/books/:id', controller.bookCtrl.getBook);
  app.put('/books/:id', controller.bookCtrl.updateBook);
  app.delete('/books/:id', controller.bookCtrl.deleteBook);

  app.post('/users', controller.userCtrl.createUser);
  app.get('/users', controller.userCtrl.listUsers);
  app.put('/users/:id', controller.userCtrl.updateUser);
  app.delete('/users/:id', controller.userCtrl.deleteUser);


}
