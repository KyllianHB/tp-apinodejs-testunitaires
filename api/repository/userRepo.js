/*
* @param {String} id        - Identifiant de l'utilisateur
* @param {String} lastName  - Nom de l'utilisateur
* @param {String} firstName - Prénom de l'utilisateur
* @param {Date}   birthDate - Date de naissance
* @param {String} address   - Adresse postale
* @param {String} phone     - Téléphone (mobile ou fixe)
* @param {String} email     - Email
*
* new User (id, lastName, firstName, birthDate, address, phone, email)
*/

export default (User) => {
    const users = [
      new User('1', 'Menvusa', 'Gérard', '1998-08-06', '3 rue du fun 44000 Nantes', '0614587598', 'gerardmenvusa@gmail.com'),
    //   new User('2', 'Goureux', 'Henry', '1985-02-12', '8 rue du travail 44000 Nantes', '0623524692', 'henrygoureux@gmail.com')
    ];
  
    const listUsers = () => {
      return users;
    };

    const generateRandomNumber = () => {
      const timestamp = new Date().getTime();
      let randomNumber = Math.floor(Math.random() * timestamp);
      if (randomNumber === 0) {
        randomNumber = 1;
      }
      return randomNumber;
    }

    const createUser = (user) => {
      users.push(new User(
        generateRandomNumber().toString(),
        user.lastName,
        user.firstName,
        user.birthDate,
        user.adress,
        user.phone,
        user.email
      ));
      return user;
    }

    const updateUser = (id, user) => {


      let foundUserIdx = 0;
      users.forEach((user, idx) => {

        if (user.id === id) {
          foundUserIdx = idx;
        }
      });
      
      if (foundUserIdx >= 0) {
        users[foundUserIdx] = new User(
          user.lastName,
          user.firstName,
          user.birthDate,
          user.adress,
          user.phone,
          user.email
        );
        return user;
      }
  
      return null;
    }

    const deleteUser = (id) => {
      let deletedUser = null;
      users.forEach((user, idx) => {
        if (user.id === id) {
          deletedUser = Object.assign({}, user);
          users.splice(idx, 1);
        }
      });
  
      return deletedUser;
    }

    return {
        listUsers,
        createUser,
        updateUser,
        deleteUser
    }
}