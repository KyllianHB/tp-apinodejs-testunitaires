import bookCtrl from './bookCtrl.js';
import userCtrl from './userCtrl.js';

export default (repository) => ({
  bookCtrl: bookCtrl(repository.bookRepo),
  userCtrl: userCtrl(repository.userRepo),
});
