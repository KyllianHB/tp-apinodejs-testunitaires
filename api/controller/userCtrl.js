
export default (userRepo) => {
  const listUsers = (_, res) => {
    res.send({
      data: userRepo.listUsers()
    });
  };

  const createUser = (req, res) => {
    const user = userRepo.createUser(req.body);

    /* 
    * Si le format de date "YYYY-MM-DD" n'est pas respecté alors renvoie le code d'erreur 422 + explications
    * sinon si le format de téléphone "(+33 ou 0 ou 0033) suivi de exactement 9 chiffres" n'est pas respecté alors renvoie le code d'erreur 422 + explications
    * sinon, renvoie 201
    */ 
    if( !(/^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])$/.test(user.birthDate))){
      res.status(422).send({
        data: 'Format de date incorrect'
      });
    } else if(!(/^(?:\+33|0|0033)[1-9](?:\d{2}){4}$/.test(user.phone))) {
      res.status(422).send({
        data: 'Format de téléphone incorrect'
      });
    } else {
      res.status(201).send({
        data: user
      });
    } 

  }

  const updateUser = (req, res) => {
    const id = req.params.id;
    const user = userRepo.updateUser(id, req.body);

    // console.log('ID : ')
    // console.log(id)
    // console.log('REQ.BODY : ')
    // console.log(req.body)


    /* 
    * Si le format de date "YYYY-MM-DD" n'est pas respecté alors renvoie le code d'erreur 422 + explications
    * sinon si le format de téléphone "(+33 ou 0 ou 0033) suivi de exactement 9 chiffres" n'est pas respecté alors renvoie le code d'erreur 422 + explications
    * sinon, renvoie 201
    */ 

    if(user) {
      if( !(/^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])$/.test(user.birthDate))){
        res.status(422).send({
          data: 'Date format not correct'
        });
      } else if(!(/^(?:\+33|0|0033)[1-9](?:\d{2}){4}$/.test(user.phone))) {
        res.status(422).send({
          data: 'Phone format not correct'
        });
      } else {
        res.status(200).send({
          data: user
        });
      } 
    } else {
      res.status(404).send({
        error: `User ${id} not found`
      });
    }
  }

  const deleteUser = (req, res) => {
    const id = req.params.id;
    const deletedUser = userRepo.deleteUser(id);

    if (deletedUser) {

      return res.status(204).send({
        meta: {
          _deleted: deletedUser
        }
      });
    }

    res.status(404).send({
      error: `User ${id} not found`
    });
  }

  return {
    listUsers,
    createUser,
    updateUser,
    deleteUser
  };
}
