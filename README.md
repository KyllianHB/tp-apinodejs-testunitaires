# Node JS - Test Driven Development 

This project is made for pedagogic purposes.

## Requirements
- NodeJS engine

## The package contains
- Express : The NodeJS server framework
- Mocha : test framework for NodeJS
- Chai, Chai-http : API test and assertions
- C8 : test coverage audit and reporting

## Use it

Install dependencies

```bash
npm i
```

Enjoy it !

## Test it

Test with coverage audit

```bash
npm test
```

Test driven developement

```bash
npm run tdd
```

Will generate a report into the terminal
For more readable reportings, check the doc : https://github.com/bcoe/c8

## Further information

Mocha test framework : https://mochajs.org/#getting-started
Chai assertions : https://www.chaijs.com/api/assert/
Test coverage options and reportings : https://github.com/bcoe/c8

##

D'abord on créer le test puis on implémente l'api, dans l'ordre : 

on créer le test
on créer les fonction dans le repo
on cable le repo avec le controller pour savoir quelle fonction on va utiliser 
ensuite, on branche la route.

ça fait : 

test
1. test

api 
2. repository
3. controller
4. router

